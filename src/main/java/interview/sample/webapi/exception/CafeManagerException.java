package interview.sample.webapi.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@EqualsAndHashCode(callSuper = true)
@Data
public class CafeManagerException extends RuntimeException {

    private final HttpStatus httpStatus;
    private final String message;

    public CafeManagerException(String name, HttpStatus httpStatus) {
        super(name);
        this.httpStatus = httpStatus;
        this.message = name;
    }
}
