package interview.sample.webapi.filter;

import interview.sample.webapi.domain.model.CafeUser;
import interview.sample.webapi.service.UserService;
import lombok.RequiredArgsConstructor;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.StringTokenizer;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Component
@RequiredArgsConstructor
public class BeforeController implements Filter {

    private final UserService userService;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;

        String authHeader = request.getHeader("Authorization");
        if (authHeader == null) {
            ((HttpServletResponse) servletResponse).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }

        StringTokenizer st = new StringTokenizer(authHeader);
        if (!st.hasMoreTokens()) {
            ((HttpServletResponse) servletResponse).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }

        String basic = st.nextToken();
        if (!basic.equalsIgnoreCase("Basic")) {
            ((HttpServletResponse) servletResponse).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }

        try {
            String credentials = new String(Base64.decodeBase64(st.nextToken()), StandardCharsets.UTF_8);
            int p = credentials.indexOf(":");
            if (p == -1) {
                ((HttpServletResponse) servletResponse).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            }

            CafeUser user = userService.getUser(credentials.substring(0, p).trim());
            if (!user.getPassword().equals(credentials.substring(p + 1).trim())) {
                ((HttpServletResponse) servletResponse).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            }

            request.setAttribute("signedInUserId", user.getId());
            filterChain.doFilter(servletRequest, servletResponse);
        } catch (Exception e) {
            ((HttpServletResponse) servletResponse).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }
}
