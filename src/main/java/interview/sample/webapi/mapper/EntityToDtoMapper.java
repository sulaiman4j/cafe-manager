package interview.sample.webapi.mapper;

import java.util.List;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

public interface EntityToDtoMapper<D, E> {
    D toDto(E entity);

    List<D> toDto(List<E> entityList);
}