package interview.sample.webapi.mapper.order;

import interview.sample.webapi.domain.constant.Message;
import interview.sample.webapi.domain.constant.ProductInOrderStatus;
import interview.sample.webapi.domain.model.CafeOrder;
import interview.sample.webapi.domain.model.Product;
import interview.sample.webapi.domain.model.ProductInOrder;
import interview.sample.webapi.dto.order.AddProductInOrderToOrderResponseDto;
import interview.sample.webapi.exception.CafeManagerException;
import interview.sample.webapi.mapper.DtoToEntityMapper;
import interview.sample.webapi.mapper.EntityToDtoMapper;
import org.mapstruct.Mapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Mapper(componentModel = "spring")
@Component
public interface AddProductInOrderToOrderResponseDtoMapper extends EntityToDtoMapper<AddProductInOrderToOrderResponseDto, ProductInOrder>, DtoToEntityMapper<AddProductInOrderToOrderResponseDto, ProductInOrder> {

    default AddProductInOrderToOrderResponseDto toDto(ProductInOrder productInOrder) {

        if (productInOrder == null) {
            throw new CafeManagerException(Message.PRODUCT_IN_ORDER_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        CafeOrder order = productInOrder.getOrder();
        if (order == null) {
            throw new CafeManagerException(Message.ORDER_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        Product product = productInOrder.getProduct();
        if (product == null) {
            throw new CafeManagerException(Message.PRODUCT_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        AddProductInOrderToOrderResponseDto responseDto = new AddProductInOrderToOrderResponseDto();
        responseDto.setId(productInOrder.getId());
        responseDto.setStatus(ProductInOrderStatus.of(productInOrder.getStatus()));
        responseDto.setAmount(productInOrder.getAmount());
        responseDto.setProductId(product.getId());
        responseDto.setOrderId(order.getId());
        return responseDto;
    }
}