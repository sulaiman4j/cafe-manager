package interview.sample.webapi.mapper.order;

import interview.sample.webapi.domain.constant.Message;
import interview.sample.webapi.domain.constant.OrderStatus;
import interview.sample.webapi.domain.model.CafeOrder;
import interview.sample.webapi.dto.order.OrderResponseDto;
import interview.sample.webapi.exception.CafeManagerException;
import interview.sample.webapi.mapper.DtoToEntityMapper;
import interview.sample.webapi.mapper.EntityToDtoMapper;
import org.mapstruct.Mapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Mapper(componentModel = "spring")
@Component
public interface EditOrderResponseDtoMapper extends EntityToDtoMapper<OrderResponseDto, CafeOrder>, DtoToEntityMapper<OrderResponseDto, CafeOrder> {

    default OrderResponseDto toDto(CafeOrder order) {

        if (order == null) {
            throw new CafeManagerException(Message.ORDER_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        OrderResponseDto responseDto = new OrderResponseDto();
        responseDto.setTableNumber(order.getTable().getTableNumber());
        responseDto.setStatus(OrderStatus.of(order.getStatus()));
        return responseDto;
    }
}