package interview.sample.webapi.mapper.table;

import interview.sample.webapi.domain.constant.Message;
import interview.sample.webapi.domain.model.CafeTable;
import interview.sample.webapi.domain.model.CafeUser;
import interview.sample.webapi.dto.table.AddWaiterToTableResponseDto;
import interview.sample.webapi.exception.CafeManagerException;
import interview.sample.webapi.mapper.DtoToEntityMapper;
import interview.sample.webapi.mapper.EntityToDtoMapper;
import org.mapstruct.Mapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Mapper(componentModel = "spring")
@Component
public interface AddWaiterToTableRequestDtoMapper extends EntityToDtoMapper<AddWaiterToTableResponseDto, CafeTable>, DtoToEntityMapper<AddWaiterToTableResponseDto, CafeTable> {

    default AddWaiterToTableResponseDto toDto(CafeTable table) {
        if (table == null) {
            throw new CafeManagerException(Message.TABLE_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        Short tableNumber = table.getTableNumber();
        if (tableNumber == null) {
            throw new CafeManagerException(Message.TABLE_NUMBER_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        CafeUser user = table.getWaiter();
        if (user == null) {
            throw new CafeManagerException(Message.USER_TYPE_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        String name = user.getName();
        if (name == null) {
            throw new CafeManagerException(Message.NAME_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        AddWaiterToTableResponseDto responseDto = new AddWaiterToTableResponseDto();
        responseDto.setTableNumber(tableNumber);
        responseDto.setWaiterName(name);
        return responseDto;
    }
}