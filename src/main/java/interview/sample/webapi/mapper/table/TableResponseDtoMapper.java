package interview.sample.webapi.mapper.table;

import interview.sample.webapi.domain.constant.Message;
import interview.sample.webapi.domain.model.CafeTable;
import interview.sample.webapi.dto.table.TableResponseDto;
import interview.sample.webapi.exception.CafeManagerException;
import interview.sample.webapi.mapper.DtoToEntityMapper;
import interview.sample.webapi.mapper.EntityToDtoMapper;
import org.mapstruct.Mapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Mapper(componentModel = "spring")
@Component
public interface TableResponseDtoMapper extends EntityToDtoMapper<TableResponseDto, CafeTable>, DtoToEntityMapper<TableResponseDto, CafeTable> {

    default TableResponseDto toDto(CafeTable cafeTable) {
        if (cafeTable == null) {
            return null;
        }

        Short tableNumber = cafeTable.getTableNumber();
        if (tableNumber == null) {
            throw new CafeManagerException(Message.TABLE_NUMBER_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        Short capacity = cafeTable.getCapacity();
        if (capacity == null) {
            throw new CafeManagerException(Message.CAPACITY_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        TableResponseDto responseDto = new TableResponseDto();
        responseDto.setTableNumber(tableNumber);
        responseDto.setCapacity(capacity);
        responseDto.setId(cafeTable.getId());
        return responseDto;
    }
}