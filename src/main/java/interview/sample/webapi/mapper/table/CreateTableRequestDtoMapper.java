package interview.sample.webapi.mapper.table;

import interview.sample.webapi.domain.constant.Message;
import interview.sample.webapi.domain.model.CafeTable;
import interview.sample.webapi.dto.table.CreateTableRequestDto;
import interview.sample.webapi.exception.CafeManagerException;
import interview.sample.webapi.mapper.DtoToEntityMapper;
import interview.sample.webapi.mapper.EntityToDtoMapper;
import org.mapstruct.Mapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Mapper(componentModel = "spring")
@Component
public interface CreateTableRequestDtoMapper extends EntityToDtoMapper<CreateTableRequestDto, CafeTable>, DtoToEntityMapper<CreateTableRequestDto, CafeTable> {

    default CafeTable toEntity(CreateTableRequestDto createTableRequestDto) {
        if (createTableRequestDto == null) {
            throw new CafeManagerException(Message.CREATE_TABLE_REQUEST_DTO_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        Short tableNumber = createTableRequestDto.getTableNumber();
        if (tableNumber == null) {
            throw new CafeManagerException(Message.TABLE_NUMBER_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        Short capacity = createTableRequestDto.getCapacity();
        if (capacity == null) {
            throw new CafeManagerException(Message.CAPACITY_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        CafeTable cafeTable = new CafeTable();
        cafeTable.setTableNumber(tableNumber);
        cafeTable.setCapacity(capacity);
        return cafeTable;
    }
}