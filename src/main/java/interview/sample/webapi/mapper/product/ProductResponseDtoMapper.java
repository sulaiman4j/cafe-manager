package interview.sample.webapi.mapper.product;

import interview.sample.webapi.domain.constant.Message;
import interview.sample.webapi.domain.model.Product;
import interview.sample.webapi.dto.product.ProductResponseDto;
import interview.sample.webapi.exception.CafeManagerException;
import interview.sample.webapi.mapper.DtoToEntityMapper;
import interview.sample.webapi.mapper.EntityToDtoMapper;
import org.mapstruct.Mapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Mapper(componentModel = "spring")
@Component
public interface ProductResponseDtoMapper extends EntityToDtoMapper<ProductResponseDto, Product>, DtoToEntityMapper<ProductResponseDto, Product> {

    default ProductResponseDto toDto(Product cafeProduct) {
        if (cafeProduct == null) {
            return null;
        }

        String title = cafeProduct.getTitle();
        if (title == null || title.isBlank()) {
            throw new CafeManagerException(Message.TITLE_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        String description = cafeProduct.getDescription();
        if (description == null || description.isBlank()) {
            throw new CafeManagerException(Message.DESCRIPTION_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        Double price = cafeProduct.getPrice();
        if (price == null) {
            throw new CafeManagerException(Message.PRICE_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        ProductResponseDto responseDto = new ProductResponseDto();
        responseDto.setId(cafeProduct.getId());
        responseDto.setTitle(title);
        responseDto.setDescription(description);
        responseDto.setPrice(price);
        return responseDto;
    }
}