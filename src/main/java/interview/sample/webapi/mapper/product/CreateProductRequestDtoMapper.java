package interview.sample.webapi.mapper.product;

import interview.sample.webapi.domain.constant.Message;
import interview.sample.webapi.domain.model.Product;
import interview.sample.webapi.dto.product.CreateProductRequestDto;
import interview.sample.webapi.exception.CafeManagerException;
import interview.sample.webapi.mapper.DtoToEntityMapper;
import interview.sample.webapi.mapper.EntityToDtoMapper;
import org.mapstruct.Mapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Mapper(componentModel = "spring")
@Component
public interface CreateProductRequestDtoMapper extends EntityToDtoMapper<CreateProductRequestDto, Product>, DtoToEntityMapper<CreateProductRequestDto, Product> {

    default Product toEntity(CreateProductRequestDto createProductRequestDto) {
        if (createProductRequestDto == null) {
            throw new CafeManagerException(Message.CAFE_PRODUCT_REQUEST_DTO_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        String title = createProductRequestDto.getTitle();
        if (title == null || title.isBlank()) {
            throw new CafeManagerException(Message.TITLE_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        String description = createProductRequestDto.getDescription();
        if (description == null || description.isBlank()) {
            throw new CafeManagerException(Message.DESCRIPTION_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        Double price = createProductRequestDto.getPrice();
        if (price == null) {
            throw new CafeManagerException(Message.PRICE_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        Product product = new Product();
        product.setTitle(title);
        product.setDescription(description);
        product.setPrice(price);
        return product;
    }
}