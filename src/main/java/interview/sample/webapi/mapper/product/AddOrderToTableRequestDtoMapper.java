package interview.sample.webapi.mapper.product;

import interview.sample.webapi.domain.constant.Message;
import interview.sample.webapi.domain.model.CafeOrder;
import interview.sample.webapi.domain.model.CafeTable;
import interview.sample.webapi.dto.table.AddOrderToTableResponseDto;
import interview.sample.webapi.exception.CafeManagerException;
import interview.sample.webapi.mapper.DtoToEntityMapper;
import interview.sample.webapi.mapper.EntityToDtoMapper;
import org.mapstruct.Mapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Mapper(componentModel = "spring")
@Component
public interface AddOrderToTableRequestDtoMapper extends EntityToDtoMapper<AddOrderToTableResponseDto, CafeTable>, DtoToEntityMapper<AddOrderToTableResponseDto, CafeTable> {

    default AddOrderToTableResponseDto toDto(CafeTable table) {

        if (table == null) {
            throw new CafeManagerException(Message.TABLE_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        CafeOrder order = table.getOrder();
        if (order == null) {
            throw new CafeManagerException(Message.ORDER_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        Short tableNumber = table.getTableNumber();
        if (tableNumber == null) {
            throw new CafeManagerException(Message.TABLE_NUMBER_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        AddOrderToTableResponseDto responseDto = new AddOrderToTableResponseDto();
        responseDto.setTableNumber(tableNumber);
        responseDto.setOrderId(order.getId());
        return responseDto;
    }
}