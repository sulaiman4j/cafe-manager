package interview.sample.webapi.mapper;

import java.util.List;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

public interface DtoToEntityMapper<D, E> {
    E toEntity(D dto);

    List<E> toEntity(List<D> dtoList);
}
