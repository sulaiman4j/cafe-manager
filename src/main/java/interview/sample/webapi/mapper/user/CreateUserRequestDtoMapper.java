package interview.sample.webapi.mapper.user;

import interview.sample.webapi.domain.constant.Message;
import interview.sample.webapi.domain.constant.UserType;
import interview.sample.webapi.domain.model.CafeUser;
import interview.sample.webapi.dto.user.CreateUserRequestDto;
import interview.sample.webapi.exception.CafeManagerException;
import interview.sample.webapi.mapper.DtoToEntityMapper;
import interview.sample.webapi.mapper.EntityToDtoMapper;
import org.mapstruct.Mapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Mapper(componentModel = "spring")
@Component
public interface CreateUserRequestDtoMapper extends EntityToDtoMapper<CreateUserRequestDto, CafeUser>, DtoToEntityMapper<CreateUserRequestDto, CafeUser> {

    default CafeUser toEntity(CreateUserRequestDto createUserRequestDto) {
        if (createUserRequestDto == null) {
            throw new CafeManagerException(Message.CAFE_USER_REQUEST_DTO_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        UserType userType = createUserRequestDto.getUserType();
        if (userType == null) {
            throw new CafeManagerException(Message.USER_TYPE_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        String name = createUserRequestDto.getName();
        if (name == null || name.isBlank()) {
            throw new CafeManagerException(Message.NAME_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        CafeUser cafeUser = new CafeUser();
        cafeUser.setName(name);
        cafeUser.setUserType(userType.type());
        cafeUser.setPassword(createUserRequestDto.getPassword());
        return cafeUser;
    }
}