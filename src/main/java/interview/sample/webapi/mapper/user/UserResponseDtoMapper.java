package interview.sample.webapi.mapper.user;

import interview.sample.webapi.domain.constant.UserType;
import interview.sample.webapi.domain.model.CafeUser;
import interview.sample.webapi.dto.user.UserResponseDto;
import interview.sample.webapi.exception.CafeManagerException;
import interview.sample.webapi.domain.constant.Message;
import interview.sample.webapi.mapper.DtoToEntityMapper;
import interview.sample.webapi.mapper.EntityToDtoMapper;
import org.mapstruct.Mapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Mapper(componentModel = "spring")
@Component
public interface UserResponseDtoMapper extends EntityToDtoMapper<UserResponseDto, CafeUser>, DtoToEntityMapper<UserResponseDto, CafeUser> {

    default UserResponseDto toDto(CafeUser cafeUser) {
        if (cafeUser == null) {
            return null;
        }

        UserType userType = UserType.of(cafeUser.getUserType());
        if (userType == null) {
            throw new CafeManagerException(Message.USER_TYPE_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        String name = cafeUser.getName();
        if (name == null || name.isBlank()) {
            throw new CafeManagerException(Message.NAME_CANNOT_BE_NULL, HttpStatus.BAD_REQUEST);
        }

        UserResponseDto responseDto = new UserResponseDto();
        responseDto.setName(name);
        responseDto.setUserType(userType);
        responseDto.setId(cafeUser.getId());
        return responseDto;
    }
}