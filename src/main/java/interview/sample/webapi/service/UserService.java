package interview.sample.webapi.service;

import interview.sample.webapi.domain.constant.Message;
import interview.sample.webapi.domain.constant.UserType;
import interview.sample.webapi.domain.model.CafeUser;
import interview.sample.webapi.repository.UserRepository;
import interview.sample.webapi.dto.table.TableResponseDto;
import interview.sample.webapi.dto.user.CreateUserRequestDto;
import interview.sample.webapi.dto.user.UserResponseDto;
import interview.sample.webapi.exception.CafeManagerException;
import interview.sample.webapi.mapper.table.TableResponseDtoMapper;
import interview.sample.webapi.mapper.user.CreateUserRequestDtoMapper;
import interview.sample.webapi.mapper.user.UserResponseDtoMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final CreateUserRequestDtoMapper createUserRequestDtoMapper;
    private final UserResponseDtoMapper userResponseDtoMapper;
    private final TableResponseDtoMapper tableResponseDtoMapper;

    public UserResponseDto createUser(Long signedInUserId, CreateUserRequestDto userRequestDto) {
        if(!UserType.of(getUser(signedInUserId).getUserType()).equals(UserType.MANAGER)){
            throw new CafeManagerException(Message.USER_CANNOT_ACCESS_TO_THIS_OPERATION, HttpStatus.FORBIDDEN);
        }

        return userResponseDtoMapper.toDto(userRepository.save(createUserRequestDtoMapper.toEntity(userRequestDto)));
    }

    public CafeUser getUser(String name) {

        Optional<CafeUser> userOptional = userRepository.findByName(name);
        if (userOptional.isEmpty()){
            throw new CafeManagerException(String.format(Message.USER_IS_NOT_EXIST, name), HttpStatus.NOT_FOUND);
        }

        return userOptional.get();
    }

    public CafeUser getUser(Long id) {

        Optional<CafeUser> userOptional = userRepository.findById(id);
        if (userOptional.isEmpty()){
            throw new CafeManagerException(String.format(Message.USER_IS_NOT_EXIST, id), HttpStatus.NOT_FOUND);
        }

        return userOptional.get();
    }

    public Page<TableResponseDto> getAssignedTables(Long id, int page, int size) {
        if(!UserType.of(getUser(id).getUserType()).equals(UserType.WAITER)){
            throw new CafeManagerException(Message.USER_CANNOT_ACCESS_TO_THIS_OPERATION, HttpStatus.FORBIDDEN);
        }

        List<TableResponseDto> tableResponseDtoList = getUser(id).getAssignedTables().stream().map(tableResponseDtoMapper::toDto).collect(Collectors.toList());
        return new PageImpl<>(tableResponseDtoList, PageRequest.of(page, size), tableResponseDtoList.size());
    }
}
