package interview.sample.webapi.service;

import interview.sample.webapi.domain.constant.Message;
import interview.sample.webapi.domain.constant.ProductInOrderStatus;
import interview.sample.webapi.domain.constant.UserType;
import interview.sample.webapi.domain.model.CafeOrder;
import interview.sample.webapi.domain.model.ProductInOrder;
import interview.sample.webapi.repository.OrderRepository;
import interview.sample.webapi.dto.order.AddProductInOrderToOrderResponseDto;
import interview.sample.webapi.dto.order.EditOrderRequestDto;
import interview.sample.webapi.dto.order.OrderResponseDto;
import interview.sample.webapi.exception.CafeManagerException;
import interview.sample.webapi.mapper.order.AddProductInOrderToOrderResponseDtoMapper;
import interview.sample.webapi.mapper.order.EditOrderResponseDtoMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Service
@RequiredArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;
    private final ProductService productService;
    private final UserService userService;
    private final ProductInOrderService productInOrderService;
    private final AddProductInOrderToOrderResponseDtoMapper addProductInOrderToOrderResponseDtoMapper;
    private final EditOrderResponseDtoMapper editOrderResponseDtoMapper;

    public CafeOrder createOrder(CafeOrder order) {
        return orderRepository.save(order);
    }

    public AddProductInOrderToOrderResponseDto addProductInOrder(Long signedInUserId, Long orderId, Long productId, Integer amount) {
        if(!UserType.of(userService.getUser(signedInUserId).getUserType()).equals(UserType.WAITER)){
            throw new CafeManagerException(Message.USER_CANNOT_ACCESS_TO_THIS_OPERATION, HttpStatus.FORBIDDEN);
        }

        ProductInOrder productInOrder = new ProductInOrder();
        productInOrder.setOrder(getOrder(orderId));
        productInOrder.setProduct(productService.getProduct(productId));
        productInOrder.setAmount(amount);
        productInOrder.setStatus(ProductInOrderStatus.ACTIVE.type());
        return addProductInOrderToOrderResponseDtoMapper.toDto(productInOrderService.createProductInOrder(productInOrder));
    }

    public CafeOrder getOrder(Long id) {
        Optional<CafeOrder> orderOptional = orderRepository.findById(id);
        if (orderOptional.isEmpty()){
            throw new CafeManagerException(String.format(Message.ORDER_IS_NOT_EXISTS, id), HttpStatus.NOT_FOUND);
        }

        return orderOptional.get();
    }

    public OrderResponseDto edit(Long signedInUserId, Long orderId, EditOrderRequestDto requestDto) {
        if(!UserType.of(userService.getUser(signedInUserId).getUserType()).equals(UserType.WAITER)){
            throw new CafeManagerException(Message.USER_CANNOT_ACCESS_TO_THIS_OPERATION, HttpStatus.FORBIDDEN);
        }

        CafeOrder order = getOrder(orderId);
        if (requestDto.getStatus() != null){
            order.setStatus(requestDto.getStatus().type());
        }

        return editOrderResponseDtoMapper.toDto(orderRepository.save(order));
    }
}
