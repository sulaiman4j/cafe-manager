package interview.sample.webapi.service;

import interview.sample.webapi.domain.constant.Message;
import interview.sample.webapi.domain.constant.OrderStatus;
import interview.sample.webapi.domain.constant.UserType;
import interview.sample.webapi.domain.model.CafeOrder;
import interview.sample.webapi.domain.model.CafeTable;
import interview.sample.webapi.domain.model.CafeUser;
import interview.sample.webapi.repository.TableRepository;
import interview.sample.webapi.dto.table.AddOrderToTableResponseDto;
import interview.sample.webapi.dto.table.AddWaiterToTableResponseDto;
import interview.sample.webapi.dto.table.CreateTableRequestDto;
import interview.sample.webapi.dto.table.TableResponseDto;
import interview.sample.webapi.exception.CafeManagerException;
import interview.sample.webapi.mapper.product.AddOrderToTableRequestDtoMapper;
import interview.sample.webapi.mapper.table.AddWaiterToTableRequestDtoMapper;
import interview.sample.webapi.mapper.table.CreateTableRequestDtoMapper;
import interview.sample.webapi.mapper.table.TableResponseDtoMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Service
@RequiredArgsConstructor
public class TableService {
    private final TableRepository tableRepository;
    private final UserService userService;
    private final OrderService orderService;
    private final CreateTableRequestDtoMapper createTableRequestDtoMapper;
    private final TableResponseDtoMapper tableResponseDtoMapper;
    private final AddWaiterToTableRequestDtoMapper addWaiterToTableRequestDtoMapper;
    private final AddOrderToTableRequestDtoMapper addOrderToTableRequestDtoMapper;

    public TableResponseDto createTable(Long signedInUserId, CreateTableRequestDto userRequestDto) {
        if(!UserType.of(userService.getUser(signedInUserId).getUserType()).equals(UserType.MANAGER)){
            throw new CafeManagerException(Message.USER_CANNOT_ACCESS_TO_THIS_OPERATION, HttpStatus.FORBIDDEN);
        }

        return tableResponseDtoMapper.toDto(tableRepository.save(createTableRequestDtoMapper.toEntity(userRequestDto)));
    }

    public AddWaiterToTableResponseDto addWaiter(Long signedInUserId, Short tableNumber, String waiterName) {
        if(!UserType.of(userService.getUser(signedInUserId).getUserType()).equals(UserType.MANAGER)){
            throw new CafeManagerException(Message.USER_CANNOT_ACCESS_TO_THIS_OPERATION, HttpStatus.FORBIDDEN);
        }

        CafeTable table = getTable(tableNumber);
        if (table.getWaiter() != null) {
            throw new CafeManagerException(Message.TABLE_ALREADY_ASSIGNED, HttpStatus.NOT_ACCEPTABLE);
        }

        CafeUser user = userService.getUser(waiterName);
        if (!Objects.requireNonNull(UserType.of(user.getUserType())).equals(UserType.WAITER)) {
            throw new CafeManagerException(Message.USER_MUST_BE_WAITER, HttpStatus.NOT_ACCEPTABLE);
        }

        table.setWaiter(user);
        return addWaiterToTableRequestDtoMapper.toDto(tableRepository.save(table));
    }

    public CafeTable getTable(Short tableNumber) {
        Optional<CafeTable> tableOptional = tableRepository.findByTableNumber(tableNumber);
        if (tableOptional.isEmpty()) {
            throw new CafeManagerException(String.format(Message.TABLE_IS_NOT_EXIST, tableNumber), HttpStatus.NOT_FOUND);
        }

        return tableOptional.get();
    }

    public AddOrderToTableResponseDto addOrder(Long signedInUserId, Short tableNumber) {
        if(!UserType.of(userService.getUser(signedInUserId).getUserType()).equals(UserType.WAITER)){
            throw new CafeManagerException(Message.USER_CANNOT_ACCESS_TO_THIS_OPERATION, HttpStatus.FORBIDDEN);
        }

        CafeTable table = getTable(tableNumber);
        if(table.getOrder() != null){
            throw new CafeManagerException(Message.ORDER_OF_TABLE_EXISTS, HttpStatus.CONFLICT);
        }

        CafeOrder order = new CafeOrder();
        order.setStatus(OrderStatus.OPEN.type());
        order.setTable(table);
        table.setOrder(orderService.createOrder(order));
        return addOrderToTableRequestDtoMapper.toDto(tableRepository.save(table));
    }
}
