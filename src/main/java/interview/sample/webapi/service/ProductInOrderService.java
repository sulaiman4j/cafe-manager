package interview.sample.webapi.service;

import interview.sample.webapi.domain.constant.Message;
import interview.sample.webapi.domain.constant.UserType;
import interview.sample.webapi.domain.model.ProductInOrder;
import interview.sample.webapi.repository.ProductInOrderRepository;
import interview.sample.webapi.dto.product_in_order.EditProductInOrderRequestDto;
import interview.sample.webapi.dto.product_in_order.ProductInOrderResponseDto;
import interview.sample.webapi.exception.CafeManagerException;
import interview.sample.webapi.mapper.product_in_order.ProductInOrderResponseDtoMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Service
@RequiredArgsConstructor
public class ProductInOrderService {
    private final UserService userService;
    private final ProductInOrderRepository productInOrderRepository;
    private final ProductInOrderResponseDtoMapper productInOrderResponseDtoMapper;

    public ProductInOrder createProductInOrder(ProductInOrder productInOrder) {
        return productInOrderRepository.save(productInOrder);
    }

    public ProductInOrderResponseDto edit(Long signedInUserId, Long productInOrderId, EditProductInOrderRequestDto requestDto) {
        if(!UserType.of(userService.getUser(signedInUserId).getUserType()).equals(UserType.WAITER)){
            throw new CafeManagerException(Message.USER_CANNOT_ACCESS_TO_THIS_OPERATION, HttpStatus.FORBIDDEN);
        }

        ProductInOrder productInOrder = getProductInOrder(productInOrderId);
        if (requestDto.getAmount() != null){
            productInOrder.setAmount(requestDto.getAmount());
        }

        if (requestDto.getStatus() != null){
            productInOrder.setStatus(requestDto.getStatus().type());
        }

        return productInOrderResponseDtoMapper.toDto(productInOrderRepository.save(productInOrder));
    }

    public ProductInOrder getProductInOrder(Long id) {

        Optional<ProductInOrder> productInOrderOptional = productInOrderRepository.findById(id);
        if (productInOrderOptional.isEmpty()){
            throw new CafeManagerException(String.format(Message.PRODUCT_IN_ORDER_NOT_EXISTS, id), HttpStatus.NOT_FOUND);
        }

        return productInOrderOptional.get();
    }
}
