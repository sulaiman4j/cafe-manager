package interview.sample.webapi.service;

import interview.sample.webapi.domain.constant.Message;
import interview.sample.webapi.domain.constant.UserType;
import interview.sample.webapi.domain.model.Product;
import interview.sample.webapi.repository.ProductRepository;
import interview.sample.webapi.dto.product.CreateProductRequestDto;
import interview.sample.webapi.dto.product.ProductResponseDto;
import interview.sample.webapi.exception.CafeManagerException;
import interview.sample.webapi.mapper.product.CreateProductRequestDtoMapper;
import interview.sample.webapi.mapper.product.ProductResponseDtoMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Service
@RequiredArgsConstructor
public class ProductService {
    private final UserService userService;
    private final ProductRepository productRepository;
    private final CreateProductRequestDtoMapper createProductRequestDtoMapper;
    private final ProductResponseDtoMapper userResponseDtoMapper;

    public ProductResponseDto createProduct(Long signedInUserId, CreateProductRequestDto userRequestDto) {
        if(!UserType.of(userService.getUser(signedInUserId).getUserType()).equals(UserType.MANAGER)){
            throw new CafeManagerException(Message.USER_CANNOT_ACCESS_TO_THIS_OPERATION, HttpStatus.FORBIDDEN);
        }

        return userResponseDtoMapper.toDto(productRepository.save(createProductRequestDtoMapper.toEntity(userRequestDto)));
    }

    public Product getProduct(Long id) {

        Optional<Product> productOptional = productRepository.findById(id);
        if (productOptional.isEmpty()){
            throw new CafeManagerException(String.format(Message.PRODUCT_IS_NOT_EXISTS, id), HttpStatus.NOT_FOUND);
        }

        return productOptional.get();
    }
}
