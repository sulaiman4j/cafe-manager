package interview.sample.webapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@SpringBootApplication
@Configuration
@EntityScan(basePackages = {"interview.sample.webapi"})
@EnableJpaRepositories("interview.sample.webapi.repository")
@EnableJpaAuditing
public class WebAPIApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebAPIApplication.class, args);
    }

}


