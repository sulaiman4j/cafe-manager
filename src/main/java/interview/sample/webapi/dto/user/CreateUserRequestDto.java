package interview.sample.webapi.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import interview.sample.webapi.domain.constant.UserType;
import lombok.Data;

import java.io.Serializable;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Data
public class CreateUserRequestDto implements Serializable {

    @JsonProperty("user_type")
    private UserType userType;
    private String name;
    private String password;
}
