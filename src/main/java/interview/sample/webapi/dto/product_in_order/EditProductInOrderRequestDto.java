package interview.sample.webapi.dto.product_in_order;

import interview.sample.webapi.domain.constant.ProductInOrderStatus;
import lombok.Data;

import java.io.Serializable;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Data
public class EditProductInOrderRequestDto implements Serializable {

    private Integer amount;
    private ProductInOrderStatus status;
}
