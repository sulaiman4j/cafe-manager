package interview.sample.webapi.dto.table;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Data
public class AddWaiterToTableResponseDto implements Serializable {

    @JsonProperty("table_number")
    private Short tableNumber;
    @JsonProperty("waiter_name")
    private String waiterName;
}
