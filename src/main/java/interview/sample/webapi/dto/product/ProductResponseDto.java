package interview.sample.webapi.dto.product;

import lombok.Data;

import java.io.Serializable;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Data
public class ProductResponseDto implements Serializable {

    private Long id;
    private String title;
    private String description;
    private Double price;
}
