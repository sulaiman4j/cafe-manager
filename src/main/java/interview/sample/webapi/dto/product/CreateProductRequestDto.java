package interview.sample.webapi.dto.product;

import lombok.Data;

import java.io.Serializable;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Data
public class CreateProductRequestDto implements Serializable {

    private String title;
    private String description;
    private Double price;
}
