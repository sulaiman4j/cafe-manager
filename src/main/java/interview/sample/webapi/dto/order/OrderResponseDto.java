package interview.sample.webapi.dto.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import interview.sample.webapi.domain.constant.OrderStatus;
import lombok.Data;

import java.io.Serializable;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Data
public class OrderResponseDto implements Serializable {

    @JsonProperty("table_number")
    private Short tableNumber;
    private OrderStatus status;
}