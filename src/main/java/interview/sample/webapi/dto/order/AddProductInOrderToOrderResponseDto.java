package interview.sample.webapi.dto.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import interview.sample.webapi.domain.constant.ProductInOrderStatus;
import lombok.Data;

import java.io.Serializable;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Data
public class AddProductInOrderToOrderResponseDto implements Serializable {

    private Long id;
    @JsonProperty("product_id")
    private Long productId;
    @JsonProperty("order_id")
    private Long orderId;
    private Integer amount;
    private ProductInOrderStatus status;
}
