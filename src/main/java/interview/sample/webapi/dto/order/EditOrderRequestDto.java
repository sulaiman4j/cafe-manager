package interview.sample.webapi.dto.order;

import interview.sample.webapi.domain.constant.OrderStatus;
import lombok.Data;

import java.io.Serializable;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Data
public class EditOrderRequestDto implements Serializable {

    private OrderStatus status;
}
