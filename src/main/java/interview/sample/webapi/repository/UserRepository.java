package interview.sample.webapi.repository;

import interview.sample.webapi.domain.model.CafeUser;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Repository
public interface UserRepository extends PagingAndSortingRepository<CafeUser, Long> {
    Optional<CafeUser> findByName(String name);
}
