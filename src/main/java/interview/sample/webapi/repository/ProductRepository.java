package interview.sample.webapi.repository;

import interview.sample.webapi.domain.model.Product;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {
}
