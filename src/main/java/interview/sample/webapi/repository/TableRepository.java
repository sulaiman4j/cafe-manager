package interview.sample.webapi.repository;

import interview.sample.webapi.domain.model.CafeTable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Repository
public interface TableRepository extends PagingAndSortingRepository<CafeTable, Long> {

    Optional<CafeTable> findByTableNumber(Short tableNumber);
}
