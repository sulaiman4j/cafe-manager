package interview.sample.webapi.repository;

import interview.sample.webapi.domain.model.CafeOrder;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @author sulaiman kadkhodaei 6/21/2021
 */

@Repository
public interface OrderRepository extends PagingAndSortingRepository<CafeOrder, Long> {
}
