package interview.sample.webapi.domain.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Entity
@Table
@Data
@EqualsAndHashCode(callSuper = false)
public class CafeTable extends AuditedEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "table_seq")
    @PrimaryKeyJoinColumn
    private Long id;

    @Column(nullable = false, unique = true)
    private Short tableNumber;

    @Column(nullable = false)
    private Short capacity;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "waiter_id")
    private CafeUser waiter;

    @OneToOne(targetEntity = CafeOrder.class, cascade = CascadeType.ALL)
    private CafeOrder order;
}
