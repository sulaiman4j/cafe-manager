package interview.sample.webapi.domain.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.util.Date;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@MappedSuperclass
@Audited
@Data
public abstract class AuditedEntity {
    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    private Date createdDate;

    @Column(nullable = false)
    @UpdateTimestamp
    private Date modifiedDate;
}