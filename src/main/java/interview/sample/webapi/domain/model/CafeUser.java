package interview.sample.webapi.domain.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Entity
@Table
@Data
@EqualsAndHashCode(callSuper = false)
public class CafeUser extends AuditedEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_seq")
    private Long id;

    @Column(nullable = false)
    private Short userType;

    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false)
    private String password;

    @OneToMany(mappedBy = "waiter", cascade = CascadeType.ALL, orphanRemoval=true, fetch = FetchType.EAGER)
    private List<CafeTable> assignedTables;
}
