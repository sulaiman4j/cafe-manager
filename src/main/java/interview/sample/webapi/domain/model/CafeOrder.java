package interview.sample.webapi.domain.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Entity
@Table
@Data
@EqualsAndHashCode(callSuper = false)
public class CafeOrder extends AuditedEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_seq")
    private Long id;

    @OneToOne(targetEntity = CafeTable.class)
    private CafeTable table;

    private Short status;
}
