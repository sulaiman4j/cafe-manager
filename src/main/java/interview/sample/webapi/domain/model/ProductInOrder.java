package interview.sample.webapi.domain.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Entity
@Table
@Data
@EqualsAndHashCode(callSuper = false)
public class ProductInOrder extends AuditedEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_in_order_seq")
    private Long id;

    @OneToOne(targetEntity = Product.class)
    private Product product;

    @OneToOne(targetEntity = CafeOrder.class)
    private CafeOrder order;

    private Integer amount;

    private Short status;
}
