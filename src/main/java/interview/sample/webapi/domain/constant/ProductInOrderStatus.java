package interview.sample.webapi.domain.constant;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author sulaiman kadkhodaei 6/21/2021
 */

@RequiredArgsConstructor
public enum ProductInOrderStatus {
    ACTIVE((short) 1),
    CANCELLED((short) 2);

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductInOrderStatus.class);
    private final Short type;

    public Short type() {
        return type;
    }

    public static ProductInOrderStatus of(Short type) {
        for (ProductInOrderStatus reserveStatus : ProductInOrderStatus.values()) {
            if (type.equals(reserveStatus.type()))
                return reserveStatus;
        }

        LOGGER.error("{} not found for ProductInOrderStatus : {}", ProductInOrderStatus.class.getName(), type);
        return null;
    }
}