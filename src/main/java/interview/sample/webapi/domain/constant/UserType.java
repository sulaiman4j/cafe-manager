package interview.sample.webapi.domain.constant;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@RequiredArgsConstructor
public enum UserType {
    MANAGER((short) 1),
    WAITER((short) 2);

    private static final Logger LOGGER = LoggerFactory.getLogger(UserType.class);
    private final Short type;

    public Short type() {
        return type;
    }

    public static UserType of(Short type) {
        for (UserType reserveStatus : UserType.values()) {
            if (type.equals(reserveStatus.type()))
                return reserveStatus;
        }

        LOGGER.error("{} not found for UserType : {}", UserType.class.getName(), type);
        return null;
    }
}