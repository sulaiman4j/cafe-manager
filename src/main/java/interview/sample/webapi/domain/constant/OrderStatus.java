package interview.sample.webapi.domain.constant;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author sulaiman kadkhodaei 6/21/2021
 */

@RequiredArgsConstructor
public enum OrderStatus {
    OPEN((short) 1),
    CANCELLED((short) 2),
    CLOSED((short) 3);

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderStatus.class);
    private final Short type;

    public Short type() {
        return type;
    }

    public static OrderStatus of(Short type) {
        for (OrderStatus reserveStatus : OrderStatus.values()) {
            if (type.equals(reserveStatus.type()))
                return reserveStatus;
        }

        LOGGER.error("{} not found for OrderStatus : {}", OrderStatus.class.getName(), type);
        return null;
    }
}