package interview.sample.webapi.domain.constant;

/**
 * @author sulaiman kadkhodaei 6/22/2021
 */

public final class Message {

    private Message() {
    }

    //USER
    public static final String CAFE_USER_REQUEST_DTO_CANNOT_BE_NULL = "CafeUserRequestDto cannot be null!";
    public static final String USER_TYPE_CANNOT_BE_NULL = "userType cannot be null!";
    public static final String USER_MUST_BE_WAITER = "user must be a waiter!";
    public static final String USER_IS_NOT_EXIST = "user with name/id: %s is not exist.";
    public static final String NAME_CANNOT_BE_NULL = "name cannot be null!";
    public static final String USER_CANNOT_ACCESS_TO_THIS_OPERATION = "user cannot access to this operation!";

    //TABLE
    public static final String CREATE_TABLE_REQUEST_DTO_CANNOT_BE_NULL = "createTableRequestDto cannot be null!";
    public static final String TABLE_CANNOT_BE_NULL = "table cannot be null!";
    public static final String TABLE_ALREADY_ASSIGNED = "table already assigned!";
    public static final String TABLE_IS_NOT_EXIST = "table with table number: %s is not exist.";
    public static final String TABLE_NUMBER_CANNOT_BE_NULL = "tableNumber cannot be null!";
    public static final String CAPACITY_CANNOT_BE_NULL = "capacity cannot be null!";

    //PRODUCT
    public static final String TITLE_CANNOT_BE_NULL = "title cannot be null!";
    public static final String PRODUCT_CANNOT_BE_NULL = "product cannot be null!";
    public static final String CAFE_PRODUCT_REQUEST_DTO_CANNOT_BE_NULL = "CafeProductRequestDto cannot be null!";
    public static final String DESCRIPTION_CANNOT_BE_NULL = "description cannot be null!";
    public static final String PRICE_CANNOT_BE_NULL = "price cannot be null!";
    public static final String PRODUCT_IS_NOT_EXISTS = "product with id: %s is not exist.";

    //ORDER
    public static final String ORDER_CANNOT_BE_NULL = "order cannot be null!";
    public static final String ORDER_OF_TABLE_EXISTS = "order of table is already exists!";
    public static final String ORDER_IS_NOT_EXISTS = "order with id: %s is not exist.";

    //PRODUCT IN ORDER
    public static final String PRODUCT_IN_ORDER_CANNOT_BE_NULL = "productInOrder cannot be null!";
    public static final String PRODUCT_IN_ORDER_NOT_EXISTS = "productInOrderOptional with id: %s is not exist.";
}
