package interview.sample.webapi.controller;

import interview.sample.webapi.dto.table.AddOrderToTableResponseDto;
import interview.sample.webapi.dto.table.AddWaiterToTableResponseDto;
import interview.sample.webapi.dto.table.CreateTableRequestDto;
import interview.sample.webapi.dto.table.TableResponseDto;
import interview.sample.webapi.service.TableService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Controller
@RequestMapping("v1/table")
@RequiredArgsConstructor
public class TableController {
    private final TableService tableService;

    @PostMapping
    public ResponseEntity<TableResponseDto> createNewTable(@RequestAttribute Long signedInUserId, @RequestBody CreateTableRequestDto requestDto) {
        return new ResponseEntity<>(tableService.createTable(signedInUserId, requestDto), HttpStatus.CREATED);
    }

    @PutMapping("{table-number}/add-waiter/{waiter-name}")
    public ResponseEntity<AddWaiterToTableResponseDto> addWaiter(@RequestAttribute Long signedInUserId, @PathVariable("table-number") Short tableNumber, @PathVariable("waiter-name") String waiterName) {
        return new ResponseEntity<>(tableService.addWaiter(signedInUserId, tableNumber, waiterName), HttpStatus.CREATED);
    }

    @PutMapping("{table-number}/add-order")
    public ResponseEntity<AddOrderToTableResponseDto> addOrder(@RequestAttribute Long signedInUserId, @PathVariable("table-number") Short tableNumber) {
        return new ResponseEntity<>(tableService.addOrder(signedInUserId, tableNumber), HttpStatus.CREATED);
    }
}
