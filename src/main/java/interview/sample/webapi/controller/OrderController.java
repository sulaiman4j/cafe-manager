package interview.sample.webapi.controller;

import interview.sample.webapi.dto.order.AddProductInOrderToOrderResponseDto;
import interview.sample.webapi.dto.order.EditOrderRequestDto;
import interview.sample.webapi.dto.order.OrderResponseDto;
import interview.sample.webapi.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Controller
@RequestMapping("v1/order")
@RequiredArgsConstructor
public class OrderController {
    private final OrderService orderService;

    @PutMapping("{order-id}/add-product-in-order/{product-id}")
    public ResponseEntity<AddProductInOrderToOrderResponseDto> addProductInOrder(@RequestAttribute Long signedInUserId,
                                                                                 @PathVariable("order-id") Long orderId,
                                                                                 @PathVariable("product-id") Long productId,
                                                                                 @RequestParam Integer amount) {

        return new ResponseEntity<>(orderService.addProductInOrder(signedInUserId, orderId, productId, amount), HttpStatus.CREATED);
    }

    @PutMapping("{order-id}")
    public ResponseEntity<OrderResponseDto> edit(@RequestAttribute Long signedInUserId, @PathVariable("order-id") Long orderId, @RequestBody EditOrderRequestDto requestDto) {
        return new ResponseEntity<>(orderService.edit(signedInUserId, orderId, requestDto), HttpStatus.OK);
    }
}
