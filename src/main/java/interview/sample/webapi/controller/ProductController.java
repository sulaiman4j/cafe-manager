package interview.sample.webapi.controller;

import interview.sample.webapi.dto.product.CreateProductRequestDto;
import interview.sample.webapi.dto.product.ProductResponseDto;
import interview.sample.webapi.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Controller
@RequestMapping("v1/product")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService userService;

    @PostMapping
    public ResponseEntity<ProductResponseDto> createNewProduct(@RequestAttribute Long signedInUserId, @RequestBody CreateProductRequestDto requestDto){
        return new ResponseEntity<>(userService.createProduct(signedInUserId, requestDto), HttpStatus.CREATED);
    }

}
