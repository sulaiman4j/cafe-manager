package interview.sample.webapi.controller;

import interview.sample.webapi.dto.table.TableResponseDto;
import interview.sample.webapi.dto.user.CreateUserRequestDto;
import interview.sample.webapi.dto.user.UserResponseDto;
import interview.sample.webapi.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Controller
@RequestMapping("v1/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @PostMapping
    public ResponseEntity<UserResponseDto> createNewUser(@RequestAttribute Long signedInUserId, @RequestBody CreateUserRequestDto requestDto){
        return new ResponseEntity<>(userService.createUser(signedInUserId, requestDto), HttpStatus.CREATED);
    }

    @GetMapping("{id}/tables")
    public ResponseEntity<Page<TableResponseDto>> getAssignedTables(@RequestAttribute Long signedInUserId, @RequestParam int page, @RequestParam int size){
        return new ResponseEntity<>(userService.getAssignedTables(signedInUserId, page, size), HttpStatus.OK);
    }
}
