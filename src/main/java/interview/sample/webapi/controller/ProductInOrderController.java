package interview.sample.webapi.controller;

import interview.sample.webapi.dto.product_in_order.EditProductInOrderRequestDto;
import interview.sample.webapi.dto.product_in_order.ProductInOrderResponseDto;
import interview.sample.webapi.service.ProductInOrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @author sulaiman kadkhodaei 6/20/2021
 */

@Controller
@RequestMapping("v1/product-in-order")
@RequiredArgsConstructor
public class ProductInOrderController {
    private final ProductInOrderService productInOrderService;

    @PutMapping("{product-in-order-id}")
    public ResponseEntity<ProductInOrderResponseDto> edit(@RequestAttribute Long signedInUserId, @PathVariable("product-in-order-id") Long productInOrderId, @RequestBody EditProductInOrderRequestDto requestDto) {
        return new ResponseEntity<>(productInOrderService.edit(signedInUserId, productInOrderId, requestDto), HttpStatus.OK);
    }
}
