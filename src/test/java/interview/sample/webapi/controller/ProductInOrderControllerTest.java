package interview.sample.webapi.controller;

import interview.sample.webapi.domain.constant.Message;
import interview.sample.webapi.domain.constant.OrderStatus;
import interview.sample.webapi.domain.constant.ProductInOrderStatus;
import interview.sample.webapi.domain.constant.UserType;
import interview.sample.webapi.domain.model.CafeOrder;
import interview.sample.webapi.domain.model.CafeTable;
import interview.sample.webapi.domain.model.CafeUser;
import interview.sample.webapi.domain.model.Product;
import interview.sample.webapi.repository.ProductInOrderRepository;
import interview.sample.webapi.repository.ProductRepository;
import interview.sample.webapi.repository.TableRepository;
import interview.sample.webapi.repository.UserRepository;
import interview.sample.webapi.dto.order.AddProductInOrderToOrderResponseDto;
import interview.sample.webapi.dto.product_in_order.EditProductInOrderRequestDto;
import interview.sample.webapi.dto.product_in_order.ProductInOrderResponseDto;
import interview.sample.webapi.exception.CafeManagerException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author sulaiman kadkhodaei 6/22/2021
 */

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProductInOrderControllerTest {

    @Autowired
    private ProductInOrderController productInOrderController;

    @Autowired
    private OrderController orderController;

    @Autowired
    private TableRepository tableRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductInOrderRepository productInOrderRepository;

    @Autowired
    private UserRepository userRepository;

    private CafeUser manager;
    private CafeUser waiter;

    @BeforeEach
    void setUp() {
        manager = new CafeUser();
        manager.setUserType(UserType.MANAGER.type());
        manager.setName("sulaiman manager");
        manager.setPassword("1234");
        manager = userRepository.save(manager);

        waiter = new CafeUser();
        waiter.setUserType(UserType.WAITER.type());
        waiter.setName("sulaiman waiter");
        waiter.setPassword("1234");
        waiter = userRepository.save(waiter);
    }

    @AfterEach
    void tearDown() {
        userRepository.deleteById(manager.getId());
        userRepository.deleteById(waiter.getId());
    }

    @Test
    void edit(){

        CafeTable table = new CafeTable();
        table.setTableNumber((short) 111);
        table.setCapacity((short) 22);
        CafeOrder order = new CafeOrder();
        order.setStatus(OrderStatus.OPEN.type());
        order.setTable(table);
        table.setOrder(order);
        table = tableRepository.save(table);
        Product product = new Product();
        product.setTitle("sulaimanix");
        product.setDescription("a good meal by chief");
        product.setPrice(20.25);
        product = productRepository.save(product);

        ResponseEntity<AddProductInOrderToOrderResponseDto> responseEntity = orderController.addProductInOrder(waiter.getId(), table.getOrder().getId(), product.getId(), 10);
        checkResponseStatusCode(HttpStatus.CREATED, responseEntity);

        AddProductInOrderToOrderResponseDto responseDto = responseEntity.getBody();
        assertNotNull(responseDto);
        ProductInOrderStatus status = responseDto.getStatus();
        assertNotNull(status);
        assertEquals(ProductInOrderStatus.ACTIVE, status);

        EditProductInOrderRequestDto requestDto = new EditProductInOrderRequestDto();
        requestDto.setAmount(30);
        requestDto.setStatus(ProductInOrderStatus.CANCELLED);

        ResponseEntity<ProductInOrderResponseDto> editResponseEntity = productInOrderController.edit(waiter.getId(), responseDto.getId(), requestDto);
        checkResponseStatusCode(HttpStatus.OK, editResponseEntity);

        ProductInOrderResponseDto editResponseDto = editResponseEntity.getBody();
        assertNotNull(editResponseDto);
        Integer amount = editResponseDto.getAmount();
        assertNotNull(amount);
        assertEquals(requestDto.getAmount(), amount);
        ProductInOrderStatus editedStatus = editResponseDto.getStatus();
        assertNotNull(editedStatus);
        assertEquals(ProductInOrderStatus.CANCELLED, editedStatus);

        productInOrderRepository.deleteById(responseDto.getId());
        productRepository.delete(product);
        tableRepository.delete(table);
    }

    @Test
    void editWhenProductInOrderNotExist(){
        checkErrorResponseStatusCode(assertThrows(CafeManagerException.class, () -> productInOrderController.edit(waiter.getId(), 152365544L, new EditProductInOrderRequestDto())),
                HttpStatus.NOT_FOUND,
                String.format(Message.PRODUCT_IN_ORDER_NOT_EXISTS, 152365544L));
    }

    private void checkErrorResponseStatusCode(CafeManagerException exception, HttpStatus status, String errorMessage) {
        assertNotNull(exception);
        HttpStatus statusCode = exception.getHttpStatus();
        assertNotNull(statusCode);
        assertEquals(status, statusCode);
        String message = exception.getMessage();
        assertNotNull(message);
        assertEquals(errorMessage, message);
    }

    private void checkResponseStatusCode(HttpStatus httpStatus, ResponseEntity<?> responseEntity) {
        assertNotNull(responseEntity);
        HttpStatus statusCode = responseEntity.getStatusCode();
        assertNotNull(statusCode);
        assertEquals(httpStatus, statusCode);
    }

}