package interview.sample.webapi.controller;

import interview.sample.webapi.domain.constant.Message;
import interview.sample.webapi.domain.constant.UserType;
import interview.sample.webapi.domain.model.CafeUser;
import interview.sample.webapi.repository.TableRepository;
import interview.sample.webapi.repository.UserRepository;
import interview.sample.webapi.dto.table.AddOrderToTableResponseDto;
import interview.sample.webapi.dto.table.AddWaiterToTableResponseDto;
import interview.sample.webapi.dto.table.CreateTableRequestDto;
import interview.sample.webapi.dto.table.TableResponseDto;
import interview.sample.webapi.dto.user.CreateUserRequestDto;
import interview.sample.webapi.dto.user.UserResponseDto;
import interview.sample.webapi.exception.CafeManagerException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author sulaiman kadkhodaei 6/22/2021
 */

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TableControllerTest {

    @Autowired
    private TableController tableController;

    @Autowired
    private UserController userController;

    @Autowired
    private TableRepository tableRepository;

    @Autowired
    private UserRepository userRepository;

    private CafeUser manager;
    private CafeUser waiter;

    @BeforeEach
    void setUp() {
        manager = new CafeUser();
        manager.setUserType(UserType.MANAGER.type());
        manager.setName("sulaiman manager");
        manager.setPassword("1234");
        manager = userRepository.save(manager);

        waiter = new CafeUser();
        waiter.setUserType(UserType.WAITER.type());
        waiter.setName("sulaiman waiter");
        waiter.setPassword("1234");
        waiter = userRepository.save(waiter);
    }

    @AfterEach
    void tearDown() {
        userRepository.deleteById(manager.getId());
        userRepository.deleteById(waiter.getId());
    }

    @Test
    void createNewTable() {
        tableRepository.deleteById(generateNewTable((short) 111, (short) 11));
    }

    @Test
    void createNewTableWhenTableNumberIsNull() {
        CreateTableRequestDto requestDto = new CreateTableRequestDto();
        requestDto.setCapacity((short) 11);
        checkErrorResponseStatusCode(assertThrows(CafeManagerException.class, () -> tableController.createNewTable(manager.getId(), requestDto)), HttpStatus.BAD_REQUEST, Message.TABLE_NUMBER_CANNOT_BE_NULL);
    }

    @Test
    void createNewTableWhenCapacityIsNull() {
        CreateTableRequestDto requestDto = new CreateTableRequestDto();
        requestDto.setTableNumber((short) 11);
        checkErrorResponseStatusCode(assertThrows(CafeManagerException.class, () -> tableController.createNewTable(manager.getId(), requestDto)), HttpStatus.BAD_REQUEST, Message.CAPACITY_CANNOT_BE_NULL);
    }

    @Test
    void createNewTableWhenTableNumberIsDuplicated() {
        Long firsTableId = generateNewTable((short) 111, (short) 11);
        CreateTableRequestDto requestDto = new CreateTableRequestDto();
        requestDto.setTableNumber((short) 111);
        requestDto.setCapacity((short) 22);
        assertThrows(DataIntegrityViolationException.class, () -> tableController.createNewTable(manager.getId(), requestDto));
        tableRepository.deleteById(firsTableId);
    }

    @Test
    void addWaiter() {
        Long firstTableId = generateNewTable((short) 111, (short) 11);

        ResponseEntity<AddWaiterToTableResponseDto> responseEntity = tableController.addWaiter(manager.getId(), (short) 111, "sulaiman waiter");
        checkResponseStatusCode(HttpStatus.CREATED, responseEntity);

        AddWaiterToTableResponseDto addWaiterToTableResponseDto = responseEntity.getBody();
        assertNotNull(addWaiterToTableResponseDto);
        Short tableNumber = addWaiterToTableResponseDto.getTableNumber();
        assertNotNull(tableNumber);
        assertEquals((short) 111, tableNumber);
        String waiterName = addWaiterToTableResponseDto.getWaiterName();
        assertNotNull(waiterName);
        assertEquals("sulaiman waiter", waiterName);

        tableRepository.deleteById(firstTableId);
    }

    @Test
    void addWaiterWhenTableIsNull() {

        checkErrorResponseStatusCode(assertThrows(CafeManagerException.class, () -> tableController.addWaiter(manager.getId(), (short) 111, "sulaiman waiter")),
                HttpStatus.NOT_FOUND,
                String.format(Message.TABLE_IS_NOT_EXIST, (short) 111));
    }

    @Test
    void addWaiterWhenUserIsNull() {
        Long firstTableId = generateNewTable((short) 111, (short) 11);

        checkErrorResponseStatusCode(assertThrows(CafeManagerException.class, () -> tableController.addWaiter(manager.getId(), (short) 111, "sulaiman waiter23")),
                HttpStatus.NOT_FOUND,
                String.format(Message.USER_IS_NOT_EXIST, "sulaiman waiter23"));

        tableRepository.deleteById(firstTableId);
    }

    @Test
    void addWaiterWhenAlreadyAdded() {
        Long firstTableId = generateNewTable((short) 111, (short) 11);

        ResponseEntity<AddWaiterToTableResponseDto> responseEntity = tableController.addWaiter(manager.getId(), (short) 111, "sulaiman waiter");
        checkResponseStatusCode(HttpStatus.CREATED, responseEntity);

        AddWaiterToTableResponseDto addWaiterToTableResponseDto = responseEntity.getBody();
        assertNotNull(addWaiterToTableResponseDto);
        Short tableNumber = addWaiterToTableResponseDto.getTableNumber();
        assertNotNull(tableNumber);
        assertEquals((short) 111, tableNumber);
        String waiterName = addWaiterToTableResponseDto.getWaiterName();
        assertNotNull(waiterName);
        assertEquals("sulaiman waiter", waiterName);

        checkErrorResponseStatusCode(assertThrows(CafeManagerException.class, () -> tableController.addWaiter(manager.getId(), (short) 111, "sulaiman waiter")),
                HttpStatus.NOT_ACCEPTABLE,
                Message.TABLE_ALREADY_ASSIGNED);

        tableRepository.deleteById(firstTableId);
    }

    @Test
    void addWaiterWhenUserIsNotWaiter() {
        Long firstTableId = generateNewTable((short) 111, (short) 11);

        checkErrorResponseStatusCode(assertThrows(CafeManagerException.class, () -> tableController.addWaiter(manager.getId(), (short) 111, "sulaiman manager")),
                HttpStatus.NOT_ACCEPTABLE,
                Message.USER_MUST_BE_WAITER);

        tableRepository.deleteById(firstTableId);
    }

    @Test
    void addOrder() {
        Long firstTableId = generateNewTable((short) 111, (short) 11);

        ResponseEntity<AddOrderToTableResponseDto> responseEntity = tableController.addOrder(waiter.getId(), (short) 111);
        checkResponseStatusCode(HttpStatus.CREATED, responseEntity);

        AddOrderToTableResponseDto responseDto = responseEntity.getBody();
        assertNotNull(responseDto);
        Short tableNumber = responseDto.getTableNumber();
        assertNotNull(tableNumber);
        assertEquals((short) 111, tableNumber);
        Long orderId = responseDto.getOrderId();
        assertNotNull(orderId);

        tableRepository.deleteById(firstTableId);
    }

    @Test
    void addOrderWhenAlreadyOrderExist() {
        Long firstTableId = generateNewTable((short) 111, (short) 11);

        ResponseEntity<AddOrderToTableResponseDto> responseEntity = tableController.addOrder(waiter.getId(), (short) 111);
        checkResponseStatusCode(HttpStatus.CREATED, responseEntity);

        AddOrderToTableResponseDto responseDto = responseEntity.getBody();
        assertNotNull(responseDto);
        Short tableNumber = responseDto.getTableNumber();
        assertNotNull(tableNumber);
        assertEquals((short) 111, tableNumber);
        Long orderId = responseDto.getOrderId();
        assertNotNull(orderId);

        checkErrorResponseStatusCode(assertThrows(CafeManagerException.class, () -> tableController.addOrder(waiter.getId(), (short) 111)),
                HttpStatus.CONFLICT,
                Message.ORDER_OF_TABLE_EXISTS);

        tableRepository.deleteById(firstTableId);
    }

    @Test
    void addOrderWhenTableIsNotExist() {

        checkErrorResponseStatusCode(assertThrows(CafeManagerException.class, () -> tableController.addOrder(waiter.getId(), (short) 111)),
                HttpStatus.NOT_FOUND,
                String.format(Message.TABLE_IS_NOT_EXIST, (short) 111));
    }

    private void checkErrorResponseStatusCode(CafeManagerException exception, HttpStatus status, String errorMessage) {
        assertNotNull(exception);
        HttpStatus statusCode = exception.getHttpStatus();
        assertNotNull(statusCode);
        assertEquals(status, statusCode);
        String message = exception.getMessage();
        assertNotNull(message);
        assertEquals(errorMessage, message);
    }

    private void checkResponseStatusCode(HttpStatus httpStatus, ResponseEntity<?> responseEntity) {
        assertNotNull(responseEntity);
        HttpStatus statusCode = responseEntity.getStatusCode();
        assertNotNull(statusCode);
        assertEquals(httpStatus, statusCode);
    }

    private Long generateNewTable(Short tableNumber, Short capacity) {
        CreateTableRequestDto requestDto = new CreateTableRequestDto();
        requestDto.setTableNumber(tableNumber);
        requestDto.setCapacity(capacity);

        ResponseEntity<TableResponseDto> responseEntity = tableController.createNewTable(manager.getId(), requestDto);
        checkResponseStatusCode(HttpStatus.CREATED, responseEntity);

        TableResponseDto tableResponseDto = responseEntity.getBody();
        Short responseDtoCapacity = tableResponseDto.getCapacity();
        assertNotNull(responseDtoCapacity);
        assertEquals(requestDto.getCapacity(), responseDtoCapacity);
        Short responseDtoTableNumber = tableResponseDto.getTableNumber();
        assertNotNull(responseDtoTableNumber);
        assertEquals(requestDto.getTableNumber(), responseDtoTableNumber);

        return tableResponseDto.getId();
    }

    private Long generateNewUser(String name, UserType userType) {
        CreateUserRequestDto requestDto = new CreateUserRequestDto();
        requestDto.setName(name);
        requestDto.setUserType(userType);

        ResponseEntity<UserResponseDto> responseEntity = userController.createNewUser(manager.getId(), requestDto);
        checkResponseStatusCode(HttpStatus.CREATED, responseEntity);

        UserResponseDto userResponseDto = responseEntity.getBody();
        String userResponseDtoName = userResponseDto.getName();
        assertNotNull(userResponseDtoName);
        assertEquals(requestDto.getName(), userResponseDtoName);
        UserType userResponseDtoUserType = userResponseDto.getUserType();
        assertNotNull(userResponseDtoUserType);
        assertEquals(requestDto.getUserType(), userResponseDtoUserType);

        return userResponseDto.getId();
    }

}