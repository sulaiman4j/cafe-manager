package interview.sample.webapi.controller;

import interview.sample.webapi.domain.constant.UserType;
import interview.sample.webapi.domain.model.CafeTable;
import interview.sample.webapi.domain.model.CafeUser;
import interview.sample.webapi.repository.TableRepository;
import interview.sample.webapi.repository.UserRepository;
import interview.sample.webapi.dto.table.TableResponseDto;
import interview.sample.webapi.dto.user.CreateUserRequestDto;
import interview.sample.webapi.dto.user.UserResponseDto;
import interview.sample.webapi.exception.CafeManagerException;
import interview.sample.webapi.domain.constant.Message;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author sulaiman kadkhodaei 6/22/2021
 */

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UserControllerTest {

    @Autowired
    private UserController userController;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TableRepository tableRepository;

    private CafeUser manager;
    private CafeUser waiter;

    @BeforeEach
    void setUp() {
        manager = new CafeUser();
        manager.setUserType(UserType.MANAGER.type());
        manager.setName("sulaiman manager");
        manager.setPassword("1234");
        manager = userRepository.save(manager);

        waiter = new CafeUser();
        waiter.setUserType(UserType.WAITER.type());
        waiter.setName("sulaiman waiter");
        waiter.setPassword("1234");
        waiter = userRepository.save(waiter);
    }

    @AfterEach
    void tearDown() {
        userRepository.deleteById(manager.getId());
        userRepository.deleteById(waiter.getId());
    }

    @Test
    void createNewUser() {
        userRepository.deleteById(generateNewUser("sulaiman manager123", UserType.MANAGER));
    }

    @Test
    void createNewUserWhenNameIsNull() {
        CreateUserRequestDto requestDto = new CreateUserRequestDto();
        requestDto.setUserType(UserType.MANAGER);
        checkErrorResponseStatusCode(assertThrows(CafeManagerException.class, () -> userController.createNewUser(manager.getId(), requestDto)), HttpStatus.BAD_REQUEST, Message.NAME_CANNOT_BE_NULL);
    }

    @Test
    void createNewUserWhenUserTypeIsNull() {
        CreateUserRequestDto requestDto = new CreateUserRequestDto();
        requestDto.setName("sulaiman manager12232");
        checkErrorResponseStatusCode(assertThrows(CafeManagerException.class, () -> userController.createNewUser(manager.getId(), requestDto)), HttpStatus.BAD_REQUEST, Message.USER_TYPE_CANNOT_BE_NULL);
    }

    @Test
    void createNewUserWhenNameIsDuplicated() {
        CreateUserRequestDto requestDto = new CreateUserRequestDto();
        requestDto.setName("sulaiman manager");
        requestDto.setUserType(UserType.WAITER);
        assertThrows(DataIntegrityViolationException.class, () -> userController.createNewUser(manager.getId(), requestDto));
    }

    @Test
    void getAssignedTables(){
        CafeTable table1 = new CafeTable();
        table1.setTableNumber((short) 111);
        table1.setCapacity((short) 11);
        table1.setWaiter(waiter);
        tableRepository.save(table1);

        CafeTable table2 = new CafeTable();
        table2.setTableNumber((short) 112);
        table2.setCapacity((short) 11);
        table2.setWaiter(waiter);
        tableRepository.save(table2);

        CafeTable table3 = new CafeTable();
        table3.setTableNumber((short) 113);
        table3.setCapacity((short) 11);
        table3.setWaiter(waiter);
        tableRepository.save(table3);

        ResponseEntity<Page<TableResponseDto>> responseEntity = userController.getAssignedTables(waiter.getId(), 0, 10);
        checkResponseStatusCode(HttpStatus.OK, responseEntity);

        Page<TableResponseDto> page = responseEntity.getBody();
        assertNotNull(page);
        List<TableResponseDto> tableResponseDtoList = page.getContent();
        assertFalse(tableResponseDtoList.isEmpty());
    }

    private void checkErrorResponseStatusCode(CafeManagerException exception, HttpStatus status, String errorMessage) {
        assertNotNull(exception);
        HttpStatus statusCode = exception.getHttpStatus();
        assertNotNull(statusCode);
        assertEquals(status, statusCode);
        String message = exception.getMessage();
        assertNotNull(message);
        assertEquals(errorMessage, message);
    }

    private void checkResponseStatusCode(HttpStatus httpStatus, ResponseEntity<?> responseEntity) {
        assertNotNull(responseEntity);
        HttpStatus statusCode = responseEntity.getStatusCode();
        assertNotNull(statusCode);
        assertEquals(httpStatus, statusCode);
    }

    private Long generateNewUser(String name, UserType userType) {
        CreateUserRequestDto requestDto = new CreateUserRequestDto();
        requestDto.setName(name);
        requestDto.setUserType(userType);
        requestDto.setPassword("12345");

        ResponseEntity<UserResponseDto> responseEntity = userController.createNewUser(manager.getId(), requestDto);
        checkResponseStatusCode(HttpStatus.CREATED, responseEntity);

        UserResponseDto userResponseDto = responseEntity.getBody();
        String userResponseDtoName = userResponseDto.getName();
        assertNotNull(userResponseDtoName);
        assertEquals(requestDto.getName(), userResponseDtoName);
        UserType userResponseDtoUserType = userResponseDto.getUserType();
        assertNotNull(userResponseDtoUserType);
        assertEquals(requestDto.getUserType(), userResponseDtoUserType);

        return userResponseDto.getId();
    }

}