package interview.sample.webapi.controller;

import interview.sample.webapi.domain.constant.Message;
import interview.sample.webapi.domain.constant.UserType;
import interview.sample.webapi.domain.model.CafeUser;
import interview.sample.webapi.repository.ProductRepository;
import interview.sample.webapi.repository.UserRepository;
import interview.sample.webapi.dto.product.CreateProductRequestDto;
import interview.sample.webapi.dto.product.ProductResponseDto;
import interview.sample.webapi.exception.CafeManagerException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author sulaiman kadkhodaei 6/22/2021
 */

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProductControllerTest {

    @Autowired
    private ProductController productController;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserRepository userRepository;

    private CafeUser manager;
    private CafeUser waiter;

    @BeforeEach
    void setUp() {
        manager = new CafeUser();
        manager.setUserType(UserType.MANAGER.type());
        manager.setName("sulaiman manager");
        manager.setPassword("1234");
        manager = userRepository.save(manager);

        waiter = new CafeUser();
        waiter.setUserType(UserType.WAITER.type());
        waiter.setName("sulaiman waiter");
        waiter.setPassword("1234");
        waiter = userRepository.save(waiter);
    }

    @AfterEach
    void tearDown() {
        userRepository.deleteById(manager.getId());
        userRepository.deleteById(waiter.getId());
    }

    @Test
    void createNewProduct() {
        productRepository.deleteById(generateNewProduct("sulaimanix", "a good meal by chief", 23.82));
    }

    @Test
    void createNewProductWhenTitleIsNull() {
        CreateProductRequestDto requestDto = new CreateProductRequestDto();
        requestDto.setDescription("description");
        requestDto.setPrice(18.2);
        checkErrorResponseStatusCode(assertThrows(CafeManagerException.class, () -> productController.createNewProduct(manager.getId(), requestDto)), HttpStatus.BAD_REQUEST, Message.TITLE_CANNOT_BE_NULL);
    }

    @Test
    void createNewProductWhenDescriptionIsNull() {
        CreateProductRequestDto requestDto = new CreateProductRequestDto();
        requestDto.setTitle("title");
        requestDto.setPrice(18.2);
        checkErrorResponseStatusCode(assertThrows(CafeManagerException.class, () -> productController.createNewProduct(manager.getId(), requestDto)), HttpStatus.BAD_REQUEST, Message.DESCRIPTION_CANNOT_BE_NULL);
    }

    @Test
    void createNewProductWhenPriceIsNull() {
        CreateProductRequestDto requestDto = new CreateProductRequestDto();
        requestDto.setTitle("title");
        requestDto.setDescription("description");
        checkErrorResponseStatusCode(assertThrows(CafeManagerException.class, () -> productController.createNewProduct(manager.getId(), requestDto)), HttpStatus.BAD_REQUEST, Message.PRICE_CANNOT_BE_NULL);
    }

    private void checkErrorResponseStatusCode(CafeManagerException exception, HttpStatus status, String errorMessage) {
        assertNotNull(exception);
        HttpStatus statusCode = exception.getHttpStatus();
        assertNotNull(statusCode);
        assertEquals(status, statusCode);
        String message = exception.getMessage();
        assertNotNull(message);
        assertEquals(errorMessage, message);
    }

    private void checkResponseStatusCode(HttpStatus httpStatus, ResponseEntity<?> responseEntity) {
        assertNotNull(responseEntity);
        HttpStatus statusCode = responseEntity.getStatusCode();
        assertNotNull(statusCode);
        assertEquals(httpStatus, statusCode);
    }

    private Long generateNewProduct(String title, String description, Double price) {
        CreateProductRequestDto requestDto = new CreateProductRequestDto();
        requestDto.setTitle(title);
        requestDto.setDescription(description);
        requestDto.setPrice(price);

        ResponseEntity<ProductResponseDto> responseEntity = productController.createNewProduct(manager.getId(), requestDto);
        checkResponseStatusCode(HttpStatus.CREATED, responseEntity);

        ProductResponseDto productResponseDto = responseEntity.getBody();
        String responseDtoTitle = productResponseDto.getTitle();
        assertNotNull(responseDtoTitle);
        assertEquals(requestDto.getTitle(), responseDtoTitle);
        String responseDtoDescription = productResponseDto.getDescription();
        assertNotNull(responseDtoDescription);
        assertEquals(requestDto.getDescription(), responseDtoDescription);
        Double responseDtoPrice = productResponseDto.getPrice();
        assertNotNull(responseDtoPrice);
        assertEquals(requestDto.getPrice(), responseDtoPrice);

        return productResponseDto.getId();
    }

}