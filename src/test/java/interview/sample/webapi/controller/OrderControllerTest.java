package interview.sample.webapi.controller;

import interview.sample.webapi.domain.constant.Message;
import interview.sample.webapi.domain.constant.OrderStatus;
import interview.sample.webapi.domain.constant.ProductInOrderStatus;
import interview.sample.webapi.domain.constant.UserType;
import interview.sample.webapi.domain.model.CafeOrder;
import interview.sample.webapi.domain.model.CafeTable;
import interview.sample.webapi.domain.model.CafeUser;
import interview.sample.webapi.domain.model.Product;
import interview.sample.webapi.repository.ProductInOrderRepository;
import interview.sample.webapi.repository.ProductRepository;
import interview.sample.webapi.repository.TableRepository;
import interview.sample.webapi.repository.UserRepository;
import interview.sample.webapi.dto.order.AddProductInOrderToOrderResponseDto;
import interview.sample.webapi.dto.order.EditOrderRequestDto;
import interview.sample.webapi.dto.order.OrderResponseDto;
import interview.sample.webapi.exception.CafeManagerException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author sulaiman kadkhodaei 6/22/2021
 */

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class OrderControllerTest {

    @Autowired
    private OrderController orderController;

    @Autowired
    private TableRepository tableRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductInOrderRepository productInOrderRepository;

    @Autowired
    private UserRepository userRepository;

    private CafeUser manager;
    private CafeUser waiter;

    @BeforeEach
    void setUp() {
        manager = new CafeUser();
        manager.setUserType(UserType.MANAGER.type());
        manager.setName("sulaiman manager");
        manager.setPassword("1234");
        manager = userRepository.save(manager);

        waiter = new CafeUser();
        waiter.setUserType(UserType.WAITER.type());
        waiter.setName("sulaiman waiter");
        waiter.setPassword("1234");
        waiter = userRepository.save(waiter);
    }

    @AfterEach
    void tearDown() {
        userRepository.deleteById(manager.getId());
        userRepository.deleteById(waiter.getId());
    }

    @Test
    void addProductInOrder() {

        CafeTable table = new CafeTable();
        table.setTableNumber((short) 111);
        table.setCapacity((short) 22);
        CafeOrder order = new CafeOrder();
        order.setStatus(OrderStatus.OPEN.type());
        order.setTable(table);
        table.setOrder(order);
        table = tableRepository.save(table);
        Product product = new Product();
        product.setTitle("sulaimanix");
        product.setDescription("a good meal by chief");
        product.setPrice(20.25);
        product = productRepository.save(product);

        ResponseEntity<AddProductInOrderToOrderResponseDto> responseEntity = orderController.addProductInOrder(waiter.getId(), table.getOrder().getId(), product.getId(), 10);
        checkResponseStatusCode(HttpStatus.CREATED, responseEntity);

        AddProductInOrderToOrderResponseDto responseDto = responseEntity.getBody();
        assertNotNull(responseDto);
        ProductInOrderStatus status = responseDto.getStatus();
        assertNotNull(status);
        assertEquals(ProductInOrderStatus.ACTIVE, status);

        productInOrderRepository.deleteById(responseDto.getId());
        productRepository.delete(product);
        tableRepository.delete(table);
    }

    @Test
    void addProductInOrderWhenProductNotExist() {

        CafeTable table = new CafeTable();
        table.setTableNumber((short) 111);
        table.setCapacity((short) 22);
        CafeOrder order = new CafeOrder();
        order.setStatus(OrderStatus.OPEN.type());
        order.setTable(table);
        table.setOrder(order);
        table = tableRepository.save(table);

        CafeTable finalTable = table;
        checkErrorResponseStatusCode(assertThrows(CafeManagerException.class, () -> orderController.addProductInOrder(waiter.getId(), finalTable.getOrder().getId(), 152365544L, 10)),
                HttpStatus.NOT_FOUND,
                String.format(Message.PRODUCT_IS_NOT_EXISTS, 152365544L));

        tableRepository.delete(table);
    }

    @Test
    void addProductInOrderWhenOrderNotExist() {
        Product product = new Product();
        product.setTitle("sulaimanix");
        product.setDescription("a good meal by chief");
        product.setPrice(20.25);
        product = productRepository.save(product);

        Product finalProduct = product;
        checkErrorResponseStatusCode(assertThrows(CafeManagerException.class, () -> orderController.addProductInOrder(waiter.getId(), 152365544L, finalProduct.getId(), 10)),
                HttpStatus.NOT_FOUND,
                String.format(Message.ORDER_IS_NOT_EXISTS, 152365544L));

        productRepository.delete(product);
    }

    @Test
    void edit(){
        EditOrderRequestDto requestDto = new EditOrderRequestDto();
        requestDto.setStatus(OrderStatus.CANCELLED);

        CafeTable table = new CafeTable();
        table.setTableNumber((short) 111);
        table.setCapacity((short) 22);
        CafeOrder order = new CafeOrder();
        order.setStatus(OrderStatus.OPEN.type());
        order.setTable(table);
        table.setOrder(order);
        table = tableRepository.save(table);
        Product product = new Product();
        product.setTitle("sulaimanix");
        product.setDescription("a good meal by chief");
        product.setPrice(20.25);
        product = productRepository.save(product);

        ResponseEntity<AddProductInOrderToOrderResponseDto> responseEntity = orderController.addProductInOrder(waiter.getId(), table.getOrder().getId(), product.getId(), 10);
        checkResponseStatusCode(HttpStatus.CREATED, responseEntity);

        AddProductInOrderToOrderResponseDto responseDto = responseEntity.getBody();
        assertNotNull(responseDto);
        ProductInOrderStatus status = responseDto.getStatus();
        assertNotNull(status);
        assertEquals(ProductInOrderStatus.ACTIVE, status);

        ResponseEntity<OrderResponseDto> editResponseEntity = orderController.edit(waiter.getId(), table.getOrder().getId(), requestDto);
        checkResponseStatusCode(HttpStatus.OK, editResponseEntity);

        OrderResponseDto editResponseDto = editResponseEntity.getBody();
        assertNotNull(editResponseDto);
        OrderStatus orderStatus = editResponseDto.getStatus();
        assertNotNull(orderStatus);
        assertEquals(requestDto.getStatus(), orderStatus);

        productInOrderRepository.deleteById(responseDto.getId());
        productRepository.delete(product);
        tableRepository.delete(table);
    }

    @Test
    void editWhenOrderNotExist(){
        EditOrderRequestDto requestDto = new EditOrderRequestDto();

        checkErrorResponseStatusCode(assertThrows(CafeManagerException.class, () -> orderController.edit(waiter.getId(), 152365544L, requestDto)),
                HttpStatus.NOT_FOUND,
                String.format(Message.ORDER_IS_NOT_EXISTS, 152365544L));

    }

    private void checkErrorResponseStatusCode(CafeManagerException exception, HttpStatus status, String errorMessage) {
        assertNotNull(exception);
        HttpStatus statusCode = exception.getHttpStatus();
        assertNotNull(statusCode);
        assertEquals(status, statusCode);
        String message = exception.getMessage();
        assertNotNull(message);
        assertEquals(errorMessage, message);
    }

    private void checkResponseStatusCode(HttpStatus httpStatus, ResponseEntity<?> responseEntity) {
        assertNotNull(responseEntity);
        HttpStatus statusCode = responseEntity.getStatusCode();
        assertNotNull(statusCode);
        assertEquals(httpStatus, statusCode);
    }

}